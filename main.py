# -*- coding: utf-8 -*-
import random
from os import system
from time import sleep
import copy
import os

dimensions = 60
#grid = [[0] * dimensions for _ in range(dimensions)]
grid = []


def glider(gg, kk):
    grid[gg + 1][kk - 1] = 1
    grid[gg + 1][kk] = 1
    grid[gg + 1][kk + 1] = 1
    grid[gg][kk + 1] = 1
    grid[gg - 1][kk] = 1


def newGrid(dimensions):
    for _ in range(dimensions):
        row = []
        for j in range(dimensions):
            if random.randrange(5) == 1:
                row.append(1)
            else:
                row.append(0)
        grid.append(row)


def drawGrid(dimensions):
    gridD = ''
    for i in range(dimensions):
        for j in range(dimensions):
            if grid[i][j] == 1:
                gridD += chr(219)
                #gridD += u"\u2588"
            else:
                gridD += ' '
                #gridD += u"\u2800"
        gridD += '\n'
    print(gridD)


def neighbors(x, y, dimensions):
    numberOfNeighbors = 0

    prevRow = (x - 1 + dimensions) % dimensions
    currentRow = x % dimensions
    nextRow = (x + 1) % dimensions

    prevCell = (y - 1 + dimensions) % dimensions
    currentCell = y % dimensions
    nextCell = (y + 1) % dimensions

    numberOfNeighbors += grid[prevRow][prevCell]
    numberOfNeighbors += grid[prevRow][currentCell]
    numberOfNeighbors += grid[prevRow][nextCell]

    numberOfNeighbors += grid[currentRow][prevCell]
    numberOfNeighbors += grid[currentRow][nextCell]

    numberOfNeighbors += grid[nextRow][prevCell]
    numberOfNeighbors += grid[nextRow][currentCell]
    numberOfNeighbors += grid[nextRow][nextCell]

    return numberOfNeighbors


def updateGrid(dimensions, grid):
    tempGrid = copy.deepcopy(grid)

    for i in range(dimensions):
        for j in range(dimensions):
            numberOfNeighbors = neighbors(i, j, dimensions)

            if grid[i][j] == 1:
                if (numberOfNeighbors < 2) or (numberOfNeighbors > 3):
                    tempGrid[i][j] = 0
            else:
                if numberOfNeighbors == 3:
                    tempGrid[i][j] = 1

    grid[:] = tempGrid[:]


newGrid(dimensions)

system('mode con: cols=' + str(dimensions) + ' lines='+str(dimensions+1))
while 1:

    drawGrid(dimensions)
    updateGrid(dimensions, grid)
    sleep(0.02)
